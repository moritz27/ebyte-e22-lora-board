EESchema Schematic File Version 4
LIBS:e22_board-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D?
U 1 1 5FE7D577
P 3450 3700
AR Path="/5FE7D577" Ref="D?"  Part="1" 
AR Path="/5FE7A97C/5FE7D577" Ref="D?"  Part="1" 
F 0 "D?" V 3489 3583 50  0000 R CNN
F 1 "LED" V 3398 3583 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3450 3700 50  0001 C CNN
F 3 "~" H 3450 3700 50  0001 C CNN
	1    3450 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5FE7D57D
P 3900 3700
AR Path="/5FE7D57D" Ref="D?"  Part="1" 
AR Path="/5FE7A97C/5FE7D57D" Ref="D?"  Part="1" 
F 0 "D?" V 3850 3900 50  0000 R CNN
F 1 "LED" V 3950 3950 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3900 3700 50  0001 C CNN
F 3 "~" H 3900 3700 50  0001 C CNN
	1    3900 3700
	0    1    1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5FE7D583
P 4250 3700
AR Path="/5FE7D583" Ref="D?"  Part="1" 
AR Path="/5FE7A97C/5FE7D583" Ref="D?"  Part="1" 
F 0 "D?" V 4289 3583 50  0000 R CNN
F 1 "LED" V 4198 3583 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4250 3700 50  0001 C CNN
F 3 "~" H 4250 3700 50  0001 C CNN
	1    4250 3700
	0    -1   -1   0   
$EndComp
Text HLabel 3300 2800 0    50   Input ~ 10
VIN
Text HLabel 7800 2800 2    50   Output ~ 10
VOUT
$Comp
L Injoinic:IP5306 U?
U 1 1 5FE7E5C2
P 5300 3350
F 0 "U?" H 5725 4165 50  0000 C CNN
F 1 "IP5306" H 5725 4074 50  0000 C CNN
F 2 "" H 5500 4150 157 0001 C CNN
F 3 "" H 5500 4150 157 0001 C CNN
	1    5300 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE7FF7F
P 3900 2500
F 0 "C?" H 4015 2546 50  0000 L CNN
F 1 "22uF" H 4015 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3938 2350 50  0001 C CNN
F 3 "~" H 3900 2500 50  0001 C CNN
	1    3900 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:L L?
U 1 1 5FE8023E
P 6600 2950
F 0 "L?" V 6550 3050 50  0000 C CNN
F 1 "1uH" V 6550 2850 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H3.5" H 6600 2950 50  0001 C CNN
F 3 "~" H 6600 2950 50  0001 C CNN
	1    6600 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5FE3547B
P 7150 3400
F 0 "C?" H 7265 3446 50  0000 L CNN
F 1 "22uF" H 7265 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7188 3250 50  0001 C CNN
F 3 "~" H 7150 3400 50  0001 C CNN
	1    7150 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE36119
P 7650 3400
F 0 "C?" H 7765 3446 50  0000 L CNN
F 1 "22uF" H 7765 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 3250 50  0001 C CNN
F 3 "~" H 7650 3400 50  0001 C CNN
	1    7650 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE36A39
P 7150 3700
F 0 "#PWR?" H 7150 3450 50  0001 C CNN
F 1 "GND" H 7155 3527 50  0000 C CNN
F 2 "" H 7150 3700 50  0001 C CNN
F 3 "" H 7150 3700 50  0001 C CNN
	1    7150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3700 7150 3600
Wire Wire Line
	7150 3600 7650 3600
Wire Wire Line
	7650 3600 7650 3550
Connection ~ 7150 3600
Wire Wire Line
	7150 3600 7150 3550
$Comp
L power:GND #PWR?
U 1 1 5FE38F9D
P 5750 3550
F 0 "#PWR?" H 5750 3300 50  0001 C CNN
F 1 "GND" H 5755 3377 50  0000 C CNN
F 2 "" H 5750 3550 50  0001 C CNN
F 3 "" H 5750 3550 50  0001 C CNN
	1    5750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3550 5750 3400
Wire Wire Line
	6150 3100 7150 3100
Wire Wire Line
	7150 3100 7150 3250
$Comp
L Device:R R?
U 1 1 5FE39E47
P 7400 3100
AR Path="/5FE39E47" Ref="R?"  Part="1" 
AR Path="/5FE7A97C/5FE39E47" Ref="R?"  Part="1" 
F 0 "R?" V 7500 3150 50  0000 L CNN
F 1 "0.5R" V 7500 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7330 3100 50  0001 C CNN
F 3 "~" H 7400 3100 50  0001 C CNN
	1    7400 3100
	0    1    1    0   
$EndComp
Text HLabel 7800 2950 2    50   Output ~ 10
BAT
Wire Wire Line
	6150 2950 6450 2950
Wire Wire Line
	6750 2950 7650 2950
Wire Wire Line
	7650 2950 7650 3100
Wire Wire Line
	7250 3100 7150 3100
Connection ~ 7150 3100
Wire Wire Line
	7650 2950 7800 2950
Connection ~ 7650 2950
Wire Wire Line
	7550 3100 7650 3100
Wire Wire Line
	7650 3250 7650 3100
Connection ~ 7650 3100
Wire Wire Line
	6150 2800 6600 2800
$Comp
L Device:C C?
U 1 1 5FE3BB25
P 7000 2600
F 0 "C?" H 7115 2646 50  0000 L CNN
F 1 "22uF" H 7115 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 2450 50  0001 C CNN
F 3 "~" H 7000 2600 50  0001 C CNN
	1    7000 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE3C2C1
P 7450 2600
F 0 "C?" H 7565 2646 50  0000 L CNN
F 1 "22uF" H 7565 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7488 2450 50  0001 C CNN
F 3 "~" H 7450 2600 50  0001 C CNN
	1    7450 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE3C576
P 6600 2600
F 0 "C?" H 6715 2646 50  0000 L CNN
F 1 "22uF" H 6715 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6638 2450 50  0001 C CNN
F 3 "~" H 6600 2600 50  0001 C CNN
	1    6600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2750 6600 2800
Connection ~ 6600 2800
Wire Wire Line
	6600 2800 7000 2800
Wire Wire Line
	7000 2750 7000 2800
Connection ~ 7000 2800
Wire Wire Line
	7000 2800 7450 2800
Wire Wire Line
	7450 2750 7450 2800
Connection ~ 7450 2800
Wire Wire Line
	7450 2800 7800 2800
Wire Wire Line
	6600 2450 6600 2300
Wire Wire Line
	6600 2300 7000 2300
Wire Wire Line
	7000 2300 7000 2450
Wire Wire Line
	7000 2300 7450 2300
Wire Wire Line
	7450 2300 7450 2450
Connection ~ 7000 2300
$Comp
L power:GND #PWR?
U 1 1 5FE3DABE
P 7000 2150
F 0 "#PWR?" H 7000 1900 50  0001 C CNN
F 1 "GND" H 7005 1977 50  0000 C CNN
F 2 "" H 7000 2150 50  0001 C CNN
F 3 "" H 7000 2150 50  0001 C CNN
	1    7000 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 2300 7000 2150
$Comp
L Device:LED D?
U 1 1 5FE3F959
P 4650 3700
AR Path="/5FE3F959" Ref="D?"  Part="1" 
AR Path="/5FE7A97C/5FE3F959" Ref="D?"  Part="1" 
F 0 "D?" V 4600 3900 50  0000 R CNN
F 1 "LED" V 4700 3950 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4650 3700 50  0001 C CNN
F 3 "~" H 4650 3700 50  0001 C CNN
	1    4650 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 3550 3450 3250
Wire Wire Line
	3450 3250 3900 3250
Wire Wire Line
	3900 3250 3900 3550
Wire Wire Line
	5300 2950 3900 2950
Wire Wire Line
	3900 2950 3900 3250
Connection ~ 3900 3250
Wire Wire Line
	4250 3550 4250 3250
Wire Wire Line
	4250 3250 4650 3250
Wire Wire Line
	4650 3250 4650 3550
Wire Wire Line
	4650 3250 4650 3100
Wire Wire Line
	4650 3100 5300 3100
Connection ~ 4650 3250
Wire Wire Line
	3450 3850 3450 4050
Wire Wire Line
	3450 4050 3900 4050
Wire Wire Line
	3900 4050 3900 3850
Wire Wire Line
	3900 4050 4250 4050
Wire Wire Line
	4250 4050 4250 3850
Connection ~ 3900 4050
Wire Wire Line
	4250 4050 4650 4050
Wire Wire Line
	4650 4050 4650 3850
Connection ~ 4250 4050
Wire Wire Line
	4650 4050 5100 4050
Wire Wire Line
	5100 4050 5100 3250
Wire Wire Line
	5100 3250 5300 3250
Connection ~ 4650 4050
$Comp
L Device:C C?
U 1 1 5FE44A5A
P 5050 2500
F 0 "C?" H 5165 2546 50  0000 L CNN
F 1 "10uF" H 5165 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5088 2350 50  0001 C CNN
F 3 "~" H 5050 2500 50  0001 C CNN
	1    5050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2800 3900 2800
Wire Wire Line
	5050 2650 5050 2800
Connection ~ 5050 2800
Wire Wire Line
	5050 2800 5300 2800
Wire Wire Line
	5050 2350 5050 2300
Wire Wire Line
	5050 2300 6600 2300
Connection ~ 6600 2300
$Comp
L Device:R R?
U 1 1 5FE4905C
P 4500 2300
AR Path="/5FE4905C" Ref="R?"  Part="1" 
AR Path="/5FE7A97C/5FE4905C" Ref="R?"  Part="1" 
F 0 "R?" V 4600 2350 50  0000 L CNN
F 1 "2R" V 4600 2150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4430 2300 50  0001 C CNN
F 3 "~" H 4500 2300 50  0001 C CNN
	1    4500 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 2650 3900 2800
Connection ~ 3900 2800
Wire Wire Line
	3900 2800 5050 2800
Wire Wire Line
	3900 2350 3900 2300
Wire Wire Line
	3900 2300 4350 2300
Wire Wire Line
	4650 2300 5050 2300
Connection ~ 5050 2300
Text HLabel 6400 3250 2    50   Output ~ 10
KEY
Wire Wire Line
	6400 3250 6150 3250
$EndSCHEMATC
